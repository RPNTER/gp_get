#!/usr/bin/env perl
use strict;
use warnings;

use IO::Prompt;
use LWP::UserAgent;
use HTTP::Request::Common qw{ POST GET };
use HTTP::Cookies;
use JSON::Tiny qw( decode_json );

my $url = "<naughty_url>";
my $POLLING_RATE = 5;
my $DST_DIR = "/tmp/GPs";

my $username = prompt('Username: ');
my $password = prompt('Password: ', -e => '*');
my $passkey = prompt('Passkey: ', -e => '*');

my $cumjar = HTTP::Cookies->new;
my $ua = LWP::UserAgent->new;
$ua->cookie_jar($cumjar);
$ua->protocols_allowed(['https']);
$ua->agent("gp_get script ($ua->_agent). Please PM $username if this script is causing any disruption.");

my $resp = $ua->request(POST("$url/ajax.php", ['action'=>'login','username'=>$username, 'password'=>$password, 'passkey'=>$passkey, 'keeplogged'=>'1']));
die if !$resp->is_success;

my %gps;
while () {
	my $ts = localtime();
	print "$ts: Polling for new GPs...\n";
		
	$resp  = $ua->request(GET("$url/torrents.php?grouping=0&order_by=gptime&json=noredirect", content_type=>'application/json'));
	my $data = decode_json($resp->content);
	my $authkey = $data->{'AuthKey'};

	foreach (@{$data->{'Movies'}}[0..4]) {
		my $t = $_->{'Torrents'}[0];
		last if (!$t->{'GoldenPopcorn'} or !(defined $t->{'FreeleechType'} and $t->{'FreeleechType'} eq "Freeleech"));

		my $id = $t->{'Id'};
		last if exists($gps{$id});
		
		print "$ts: Downloading $t->{'ReleaseName'} ($id)\n";
		my $tmp = "$url/torrents.php?action=download&id=$id&authkey=$authkey&torrent_pass=$passkey";
		$ua->request(GET($tmp), "$DST_DIR/$id.torrent");
		$gps{$id} = 1;
	}

	sleep $POLLING_RATE * 60;
}

# vim: ts=2
